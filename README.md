# OpenML dataset: nyc-taxi-green-dec-2016

https://www.openml.org/d/44050

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark,  
                                  transformed in the same way. This dataset belongs to the "regression on categorical and
                                  numerical features" benchmark. Original description: 
 
String datetime information extracted to numeric columns.Trip Record Data provided by the New York City Taxi and Limousine Commission (TLC) [http://www.nyc.gov/html/tlc/html/about/trip_record_data.shtml]. The dataset includes TLC trips of the green line in December 2016. Data was downloaded on 03.11.2018. For a description of all variables in the dataset checkout the TLC homepage [http://www.nyc.gov/html/tlc/downloads/pdf/data_dictionary_trip_records_green.pdf]. The variable 'tip_amount' was chosen as target variable. The variable 'total_amount' is ignored by default, otherwise the target could be predicted deterministically. The date variables 'lpep_pickup_datetime' and 'lpep_dropoff_datetime' (ignored by default) could be used to compute additional time features. In this version, we chose only trips with 'payment_type' == 1 (credit card), as tips are not included for most other payment types. We also removed the variables 'trip_distance' and 'fare_amount' to increase the importance of the categorical features 'PULocationID' and 'DOLocationID'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44050) of an [OpenML dataset](https://www.openml.org/d/44050). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44050/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44050/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44050/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

